<footer class="main-footer">
    <div class="footer-container-columns global-container">

        <div class="footer-column">
            <div class="footer-column-heading"><h2>Links</h2></div>
            <ul>
                <li><a class="simple-link" href=# >  About </a></li>
                <li><a class="simple-link" href=# >  Tutorials </a></li>
                <li><a class="simple-link" href=# >  Project </a></li>
            </ul>
        </div>
    

        <div class="footer-column">
            <div class="footer-column-heading"><h2>Info & contacts</h2></div>
            <ul>
                <li><a class="simple-link" href="mailto:okosek21@gmail.com" >  <i class="fa-solid fa-envelope fa-lg"></i> okosek21@gmail.com</a></li>
                <li><a class="simple-link" href=https://www.google.com/maps/place/Praha/ target=_blank >  <i class="fa-solid fa-location-dot fa-lg"></i> Prague, Czech Republic </a></li>
                <li><a class="simple-link" href="http://www.koyda.eu" > <i class="fa-solid fa-globe fa-lg"></i> KoYdA.eu </a></li>
            </ul>
        </div>

        <div class="footer-column">
            <div class="footer-column-heading"><h2>Social media</h2></div>
            <ul>
                <li> <a class="simple-link youtube" href="https://www.youtube.com/" target="_blank">  <i class="fa-brands fa-youtube fa-lg"></i> YouTube</a></li>
                <li> <a class="simple-link linkedin" href="https://www.linkedin.com/" target="_blank">  <i class="fa-brands fa-linkedin fa-lg"></i> Linkedin</a></li>
            </ul>
        </div>

        <div class="footer-column">
            <div class="footer-logo-container">
                <img src="src/images/logo_icon_small.png" alt="Logo KoYdA">
            </div>
        </div>

    </div>

    <div class="footer-copyright">
        <div class="footer-copyright-contanier global-container">
            <div class="footer-copyright-item copyright-left">  Powered by Racism</div>
            <div class="footer-copyright-item copyright-center"> &copy; <?php $copyYear = 2021; $curYear = date('Y'); echo $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');?> KoYdA.eu </div>
            <div class="footer-copyright-item copyright-right">  Designed by Ondřej Košek </div>
        </div>
    </div>
</footer>