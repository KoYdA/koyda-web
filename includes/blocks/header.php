<header class="main-header global-container">

<div class="header-logo-container"><img src="src/images/text-logo.png" alt=""></div>

<nav class="nav-bar">
    
    <div class="menu-button hamburger-button" onclick="hideMenu()">
    <i class="fa-solid fa-bars"></i>
    </div>

    <div class="nav-content" id="main-nav">
    <ul>
        <li> <a href="index.php"> <div class="menu-button"> About </div> </a> </li>
        <li> <a href="tutorials.php"> <div class="menu-button"> Tutorials </div> </a> </li>

        <li>
            <div class="menu-dropdown">
                <a href="#">  
                <div class="menu-button">
                    Projects
                </div>     
                </a>
                <div class="menu-dropdown-content"> 
                        <ul>
                            <li> <a href="rate.php"> <div class="menu-button"> Rate </div> </a> </li>
                            <li> <a href="#"> <div class="menu-button"> Some link 1 </div> </a> </li>
                            <li> <a href="#"> <div class="menu-button"> Some link 1 </div> </a> </li>
                        </ul>
        
                </div>
            </div> 
        </li>

        <li> <a href=""> <div class="menu-button"> My Cv </div> </a> </li>
    </ul>
    </div>

</nav>

<script src="src/js/hideMenu.js"></script>

</header>