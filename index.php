<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KoYdA</title>
    <link rel="stylesheet" href="dist/main.css">
    <script src="https://kit.fontawesome.com/1bcd673711.js" crossorigin="anonymous"></script>
</head>

<body>

    <?php include('includes/blocks/header.php'); ?>

    <div class="global-container">
    <article class="general-article">

        <h1>This is h1</h1>

        <img src="src/images/random.jpg" alt="">
        <p> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Totam, dolorum veniam dolor laudantium in eligendi rem assumenda ratione rerum cum minus. Ullam voluptates voluptatem distinctio animi velit saepe, nam fugiat!
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque soluta deserunt iste eos culpa adipisci, aliquid illo nostrum numquam provident ea saepe in temporibus esse vitae reiciendis accusamus voluptatem?
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora exercitationem, iste saepe repudiandae minima blanditiis dignissimos cupiditate impedit, sunt laborum sint obcaecati molestias rerum ipsum maxime neque quaerat accusamus quis!
        Laboriosam itaque fuga dolor ducimus sed enim quis qui ut odit ipsam. Vel non dolor qui illum enim aperiam, delectus dolore recusandae voluptas veritatis nemo similique. Ut placeat doloribus veniam?
        Recusandae aliquid at animi autem. Explicabo placeat facilis fugiat aliquid animi itaque iusto reiciendis harum magnam, nisi cupiditate deserunt dolor culpa vero recusandae illum temporibus nemo voluptas maxime dolorem perspiciatis.
        </p>

        <h2>This is h2</h2>
        <img src="src/images/random.jpg" alt="">
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Totam, dolorum veniam dolor laudantium in eligendi rem assumenda ratione rerum cum minus. Ullam voluptates voluptatem distinctio animi velit saepe, nam fugiat!
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque soluta deserunt iste eos culpa adipisci, aliquid illo nostrum numquam provident ea saepe in temporibus esse vitae reiciendis accusamus voluptatem?
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora exercitationem, iste saepe repudiandae minima blanditiis dignissimos cupiditate impedit, sunt laborum sint obcaecati molestias rerum ipsum maxime neque quaerat accusamus quis!
        Laboriosam itaque fuga dolor ducimus sed enim quis qui ut odit ipsam. Vel non dolor qui illum enim aperiam, delectus dolore recusandae voluptas veritatis nemo similique. Ut placeat doloribus veniam?
        Recusandae aliquid at animi autem. Explicabo placeat facilis fugiat aliquid animi itaque iusto reiciendis harum magnam, nisi cupiditate deserunt dolor culpa vero recusandae illum temporibus nemo voluptas maxime dolorem perspiciatis.
        </p>

        <h2>This is h2</h2>
        <img src="src/images/random.jpg" alt="">
        <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Totam, dolorum veniam dolor laudantium in eligendi rem assumenda ratione rerum cum minus. Ullam voluptates voluptatem distinctio animi velit saepe, nam fugiat!
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque soluta deserunt iste eos culpa adipisci, aliquid illo nostrum numquam provident ea saepe in temporibus esse vitae reiciendis accusamus voluptatem?
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora exercitationem, iste saepe repudiandae minima blanditiis dignissimos cupiditate impedit, sunt laborum sint obcaecati molestias rerum ipsum maxime neque quaerat accusamus quis!
        Laboriosam itaque fuga dolor ducimus sed enim quis qui ut odit ipsam. Vel non dolor qui illum enim aperiam, delectus dolore recusandae voluptas veritatis nemo similique. Ut placeat doloribus veniam?
        Recusandae aliquid at animi autem. Explicabo placeat facilis fugiat aliquid animi itaque iusto reiciendis harum magnam, nisi cupiditate deserunt dolor culpa vero recusandae illum temporibus nemo voluptas maxime dolorem perspiciatis.
        </p>
    </article>
    </div>

    <?php include('includes/blocks/footer.php'); ?>

</body>
</html>