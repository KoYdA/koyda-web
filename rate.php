<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KoYdA</title>
    <link rel="stylesheet" href="dist/main.css">
    <script src="https://kit.fontawesome.com/1bcd673711.js" crossorigin="anonymous"></script>
</head>

<body>

    <?php include('includes/blocks/header.php'); ?>

    <div class="global-container">
    <article class="general-article">

        <h1>Rating of our website</h1>

        <h2>AVG Rate from today</h2>

        <div class="banner-statistics">
            AVG RATE
            <div class="banner-statistics-content">

                <?php include('db-rate.php'); ?>
                
            </div>
        </div>

        <h2>Give us rate</h2>

        <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>"> Rate: <input type="number" name="rate">
        <input type="submit">
        </form>

        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // collect value of input field
            $rate = $_POST['rate'];
            if (empty($rate)) {
                echo "rate is empty";
            } else {
                echo $rate;
                
                include('db-rate-insert.php');

            }
            }
        ?>
       
    </article>
    </div>

    <?php include('includes/blocks/footer.php'); ?>

</body>
</html>